#define REDL 2
#define YELLOWL 3
#define GREENL 4
#define WHITEL 5
#define REDB 9
#define YELLOWB 10
#define GREENB 11
#define WHITEB 12
#define EASYB 13
#define BUZZER 6
#define RANDOM_PORT 0
#define UNDEFINED -1

#define  c3    7634
#define  d3    6803
#define  e3    6061
#define  f3    5714

#define BTN_CLICK_DELAY 100
#define SEQUENCE_MAX_SIZE 100
#define MIN_LED_TIME 250
#define START_SPEED 650
#define DIFFICULT_RATE 5
#define SPEED_UP 100

enum STATE {
  PLAY_LIGHT_SEQUENCE_STATE,
  ANSWER_TIME_STATE,
  END_GAME_STATE
};

enum DIFFICULT {
  EASY,
  DRAGON
};
//Start Melody
int startMelody[] = {262,   0, 196,   0, 196,   0, 220,   0, 196,   0, 247,   0, 262};
int startBeats[]  = {250, 325, 125, 162, 125, 162, 250, 325, 250, 575, 250, 325, 250};

//sequence
int lights[] = {REDL, YELLOWL, GREENL, WHITEL};
int notes[] = {d3, f3, e3, c3};
int sequence[SEQUENCE_MAX_SIZE];
int sequenceSize = 0;
int speed = START_SPEED;
int difficult = 0;
int difficultLevel = EASY;

//answer validadion
int checkAnswer = 0;

//program state
int state = PLAY_LIGHT_SEQUENCE_STATE;

void playMelody (int melody[], int beats[], int size) {
  for (int i = 0; i < size; i++) {
    int toneM = melody[i];
    int duration = beats[i];

    if (toneM == 0) {
      delay(duration);
    } else {
      digitalWrite(REDL, HIGH);
      digitalWrite(YELLOWL, HIGH);
      digitalWrite(GREENL, HIGH);
      digitalWrite(WHITEL, HIGH);
      playTone(toneM, duration);
      digitalWrite(REDL, LOW);
      digitalWrite(YELLOWL, LOW);
      digitalWrite(GREENL, LOW);
      digitalWrite(WHITEL, LOW);
    }
  }
}

void playTone(int toneM, long duration) {
  tone(BUZZER, toneM, duration);
  delay(duration);
}

void getDifficult () {
  difficultLevel = digitalRead(EASYB) == LOW ? EASY : DRAGON;
  Serial.println(difficultLevel);
}

void playStartSequence () {
  playMelody(startMelody, startBeats, sizeof(startMelody) / sizeof(int));
}

void playEndGameSequence () {
  digitalWrite(REDL, HIGH);
  digitalWrite(YELLOWL, HIGH);
  digitalWrite(GREENL, HIGH);
  digitalWrite(WHITEL, HIGH);
  delay(500);
  digitalWrite(REDL, LOW);
  digitalWrite(YELLOWL, LOW);
  digitalWrite(GREENL, LOW);
  digitalWrite(WHITEL, LOW);
  delay(500);
}

void playLightSequence () {
  if (verifyIfPlayerWins()) {
    state = END_GAME_STATE;
    return;
  }
  addSequence();
  difficultUp();
  playSequence();
  state = ANSWER_TIME_STATE;
}

boolean verifyIfPlayerWins () {
  return sequenceSize == SEQUENCE_MAX_SIZE;
}

void addSequence () {
  if (difficultLevel == EASY) {
    addSequences(1);
  } else {
    addSequences(4);
  }
}

void addSequences(int qtd) {
  for (int i = 0; i < qtd; i++) {
    sequence[sequenceSize] = random(0, 4);
    sequenceSize++;
  }
}

void difficultUp () {
  if (speed > MIN_LED_TIME &&  sequenceSize == difficult + DIFFICULT_RATE) {
    difficult += DIFFICULT_RATE;
    speed -= SPEED_UP;
  }
}

void playSequence() {
  for (int i = 0; i < sequenceSize; i++) {
    blinkLed(lights[sequence[i]], speed, speed);
  }
}

void capturePlayerAnswer () {
  int answer = getPressedButton();
  if (answer == UNDEFINED) {
    return;
  }

  if (isCorrectAnswer(answer)) {
    if (isAllSequenceAnswered()) {
      checkAnswer = 0;
      state = PLAY_LIGHT_SEQUENCE_STATE;
      delay(500);
    } else {
      checkAnswer++;
    }
  } else {
    state = END_GAME_STATE;
  }
}

int getPressedButton () {
  if (digitalRead(REDB) == 0) {
    blinkLed(REDL, speed, BTN_CLICK_DELAY);
    return 0;
  }
  if (digitalRead(YELLOWB) == 0) {
    blinkLed(YELLOWL, speed, BTN_CLICK_DELAY);
    return 1;
  }
  if (digitalRead(GREENB) == 0) {
    blinkLed(GREENL, speed, BTN_CLICK_DELAY);
    return 2;
  }
  if (digitalRead(WHITEB) == 0) {
    blinkLed(WHITEL, speed, BTN_CLICK_DELAY);
    return 3;
  }

  return -1;
}

boolean isCorrectAnswer (int answer) {
  return sequence[checkAnswer] == answer;
}

boolean isAllSequenceAnswered () {
  return checkAnswer + 1 == sequenceSize;
}

void blinkLed (int port, int onTime) {
  blinkLed(port, onTime, 250);
}

void blinkLed (int port, int onTime, int offTime) {
  digitalWrite(port, HIGH);
  playSound(port, onTime);
  delay(onTime);
  digitalWrite(port, LOW);
  delay(offTime);
}

void playSound(int port, int time) {
  switch (port) {
    case REDL:
      tone(BUZZER, notes[0], time);
      break;
    case YELLOWL:
      tone(BUZZER, notes[1], time);
      break;
    case GREENL:
      tone(BUZZER, notes[2], time);
      break;
    case WHITEL:
      tone(BUZZER, notes[3], time);
      break;
  }
}

void setup() {

  pinMode(REDL, OUTPUT);
  pinMode(YELLOWL, OUTPUT);
  pinMode(GREENL, OUTPUT);
  pinMode(WHITEL, OUTPUT);
  pinMode(REDB, INPUT_PULLUP);
  pinMode(YELLOWB, INPUT_PULLUP);
  pinMode(GREENB, INPUT_PULLUP);
  pinMode(WHITEB, INPUT_PULLUP);
  pinMode(EASYB, INPUT_PULLUP);
  pinMode(BUZZER, OUTPUT);

  randomSeed(analogRead(RANDOM_PORT));

  playStartSequence();
  getDifficult();
  delay(1000);
}

void loop() {
  switch (state) {
    case END_GAME_STATE :
      playEndGameSequence();
      break;
    case ANSWER_TIME_STATE :
      capturePlayerAnswer();
      break;
    case PLAY_LIGHT_SEQUENCE_STATE :
      playLightSequence();
      break;
  }
}

