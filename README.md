**Bem Vindo!**

*No genius-simple/genius-simple.ino* você encontra a versão simplificada do projeto.
Para construir este projeto você precisa de 4 leds, 4 botões e 4 resistores de 220.

*No genius-buzzer/genius-buzzer.ino* você encontra a versão simplificada do projeto.
Para construir este projeto você precisa de 4 leds, 4 botões, 4 resistores de 220, uma chave 3 terminais e um buzzer.

*No btn_test/btn_test.ino* contém um teste de acionamento dos botões.

*No buzzer-test/starwars.ino* contém o teste do buzzer com a marcha imperial e a música tema do Star Wars.

Agradeço a visita e boa diversão.

Você pode me acompanhar nas redes sociais tbm!

FACEBOOK: https://www.facebook.com/jogoNerd/

YOUTUBE: https://www.youtube.com/channel/UCPT2Ew3uWarF0QQNxr9CCbQ

INSTAGRAN: https://www.instagram.com/jogonerdoficial/

TWITTER: https://twitter.com/JogoNerdOficial

---