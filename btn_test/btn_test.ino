#define REDL 2
#define YELLOWL 3
#define GREENL 4
#define WHITEL 5
#define REDB 8
#define YELLOWB 9
#define GREENB 10
#define WHITEB 11

void setup() {
  pinMode(REDL, OUTPUT);
  pinMode(YELLOWL, OUTPUT);
  pinMode(GREENL, OUTPUT);
  pinMode(WHITEL, OUTPUT);
  pinMode(REDB, INPUT_PULLUP);
  pinMode(YELLOWB, INPUT_PULLUP);
  pinMode(GREENB, INPUT_PULLUP);
  pinMode(WHITEB, INPUT_PULLUP);
}

void loop() {
  int btnStatus1 = digitalRead(WHITEB);
  if (btnStatus1 == 0) {
    digitalWrite(WHITEL, HIGH);
  } else {
    digitalWrite(WHITEL, LOW);
  }
  int btnStatus2 = digitalRead(YELLOWB);
  if (btnStatus2 == 0) {
    digitalWrite(YELLOWL, HIGH);
  } else {
    digitalWrite(YELLOWL, LOW);
  }
  int btnStatus3 = digitalRead(GREENB);
  if (btnStatus3 == 0) {
    digitalWrite(GREENL, HIGH);
  } else {
    digitalWrite(GREENL, LOW);
  }
  int btnStatus4 = digitalRead(REDB);
  if (btnStatus4 == 0) {
    digitalWrite(REDL, HIGH);
  } else {
    digitalWrite(REDL, LOW);
  }
}
