#define REDL 2
#define YELLOWL 3
#define GREENL 4
#define WHITEL 5
#define REDB 8
#define YELLOWB 9
#define GREENB 10
#define WHITEB 11
#define UNDEFINED -1
#define RANDOM_PORT 0

#define BTN_CLICK_DELAY 100
#define SEQUENCE_MAX_SIZE 50
#define MIN_LED_TIME 250
#define START_SPEED 650
#define DIFFICULT_RATE 5
#define SPEED_UP 100

enum STATE {
  PLAY_LIGHT_SEQUENCE_STATE,
  ANSWER_TIME_STATE,
  END_GAME_STATE
};

//sequence
int lights[] = {REDL, YELLOWL, GREENL, WHITEL};
int sequence[SEQUENCE_MAX_SIZE];
int sequenceSize = 0;
int speed = START_SPEED;
int difficult = 0;

//answer validadion
int checkAnswer = 0;

//program state
int state = PLAY_LIGHT_SEQUENCE_STATE;

void setup() {
  pinMode(REDL, OUTPUT);
  pinMode(YELLOWL, OUTPUT);
  pinMode(GREENL, OUTPUT);
  pinMode(WHITEL, OUTPUT);
  pinMode(REDB, INPUT_PULLUP);
  pinMode(YELLOWB, INPUT_PULLUP);
  pinMode(GREENB, INPUT_PULLUP);
  pinMode(WHITEB, INPUT_PULLUP);

  randomSeed(analogRead(RANDOM_PORT));

  playStartSequence();
  delay(1000);
}

void loop() {
  switch (state) {
    case END_GAME_STATE :
      playEndGameSequence();
      break;
    case ANSWER_TIME_STATE :
      capturePlayerAnswer();
      break;
    case PLAY_LIGHT_SEQUENCE_STATE :
      playLightSequence();
      break;
  }
}

void playStartSequence () {
  for (int i = 0; i < 10; i++) {
    blinkAll(100);
  }
}

void playEndGameSequence () {
  blinkAll(500);
}

void playLightSequence () {
  if (verifyIfPlayerWins()) {
    state = END_GAME_STATE;
    return;
  }
  addSequence();
  difficultUp();
  playSequence();
  state = ANSWER_TIME_STATE;
}

boolean verifyIfPlayerWins () {
  return sequenceSize == SEQUENCE_MAX_SIZE;
}

void addSequence () {
  sequence[sequenceSize] = random(0, 4);
  sequenceSize++;
}

void difficultUp () {
  if (speed > MIN_LED_TIME &&  sequenceSize == difficult + DIFFICULT_RATE) {
    difficult += DIFFICULT_RATE;
    speed -= SPEED_UP;
  }
}

void playSequence() {
  for (int i = 0; i < sequenceSize; i++) {
    blinkLed(lights[sequence[i]], speed, speed);
  }
}

void capturePlayerAnswer () {
  int answer = getPressedButton();
  if (answer == UNDEFINED) {
    return;
  }

  if (isCorrectAnswer(answer)) {
    if (isAllSequenceAnswered()) {
      checkAnswer = 0;
      state = PLAY_LIGHT_SEQUENCE_STATE;
      delay(500);
    } else {
      checkAnswer++;
    }
  } else {
    state = END_GAME_STATE;
  }
}

int getPressedButton () {
  if (digitalRead(REDB) == 0) {
    blinkLed(REDL, speed, BTN_CLICK_DELAY);
    return 0;
  }
  if (digitalRead(YELLOWB) == 0) {
    blinkLed(YELLOWL, speed, BTN_CLICK_DELAY);
    return 1;
  }
  if (digitalRead(GREENB) == 0) {
    blinkLed(GREENL, speed, BTN_CLICK_DELAY);
    return 2;
  }
  if (digitalRead(WHITEB) == 0) {
    blinkLed(WHITEL, speed, BTN_CLICK_DELAY);
    return 3;
  }

  return -1;
}

boolean isCorrectAnswer (int answer) {
  return sequence[checkAnswer] == answer;
}

boolean isAllSequenceAnswered () {
  return checkAnswer + 1 == sequenceSize;
}

void blinkAll (int time) {
  digitalWrite(REDL, HIGH);
  digitalWrite(YELLOWL, HIGH);
  digitalWrite(GREENL, HIGH);
  digitalWrite(WHITEL, HIGH);
  delay(time);
  digitalWrite(REDL, LOW);
  digitalWrite(YELLOWL, LOW);
  digitalWrite(GREENL, LOW);
  digitalWrite(WHITEL, LOW);
  delay(time);
}

void blinkLed (int port, int onTime) {
  blinkLed(port, onTime, 250);
}

void blinkLed (int port, int onTime, int offTime) {
  digitalWrite(port, HIGH);
  delay(onTime);
  digitalWrite(port, LOW);
  delay(offTime);
}

