/**
   Arduino-buzzer Star Wars Threme
   A fonte original https://github.com/AbhishekGhosh/Arduino-Buzzer-Tone-Codes/blob/master/star-wars.ino
   Dei uma simplificada do código original
   Divirtam-se!
*/
#define  c3    7634
#define  d3    6803
#define  e3    6061
#define  f3    5714
#define  g3    5102
#define  a3    4545
#define  b3    4049
#define  c4    3816    // 261 Hz 
#define  d4    3401    // 294 Hz 
#define  e4    3030    // 329 Hz 
#define  f4    2865    // 349 Hz 
#define  g4    2551    // 392 Hz 
#define  a4    2272    // 440 Hz 
#define  a4s   2146
#define  b4    2028    // 493 Hz 
#define  c5    1912    // 523 Hz
#define  d5    1706
#define  d5s   1608
#define  e5    1517    // 659 Hz
#define  f5    1433    // 698 Hz
#define  g5    1276
#define  a5    1136
#define  a5s   1073
#define  b5    1012
#define  c6    955

#define  R     0
#define BUZZER 6

#define INTERVAL 3000

// MELODIES and TIMING //
// Melody 1: Star Wars Imperial March
int imperialMarchMelody[] = { a4,   R,  a4,   R,  a4,   R,  f4,  R,  c5,  R,  a4,   R,  f4,  R,  c5,  R,  a4,   R,  e5,   R,  e5,   R,  e5,   R,  f5, R,   c5,  R,  g5,   R,  f5,  R,  c5,  R,  a4,   R};
int imperialMarchBeats[]  = {500, 200, 500, 200, 500, 200, 400, 50, 200, 50, 600, 100, 400, 50, 200, 50, 600, 800, 500, 200, 500, 200, 500, 200, 400, 50, 200, 50, 600, 100, 400, 50, 200, 50, 600, 400};
// Melody 2: Star Wars Theme
int starWarsThemeMelody[] = { f4,  f4,  f4,  a4s,   f5, d5s,  d5,  c5,  a5s,  f5, d5s,  d5,  c5,  a5s,  f5, d5s,  d5, d5s,  c5};
int starWarsThemeBeats[]  = {210, 210, 210, 1280, 1280, 210, 210, 210, 1280, 640, 210, 210, 210, 1280, 640, 210, 210, 210, 1280};

void playImperialMarch () {
  playMelody(imperialMarchMelody, imperialMarchBeats, sizeof(imperialMarchBeats) / sizeof(int));
}

void playStarWarsTheme () {
  playMelody(starWarsThemeMelody, starWarsThemeBeats, sizeof(starWarsThemeBeats) / sizeof(int));
}

void playMelody (int melody[], int beats[], int size) {
  Serial.print("Melody Size: ");
  Serial.println(size);
  
  for (int i = 0; i < size; i++) {
    int toneM = melody[i];
    int duration = beats[i];
    
    Serial.print("tone: ");
    Serial.print(toneM);
    Serial.print(" beat-time: ");
    Serial.print(duration);
    Serial.println();
    playTone(toneM, duration);
  }
}

void playTone(int toneM, long duration) {
  tone(BUZZER, toneM, duration);
  delay(duration);
}

void setup() {
  Serial.begin(9600);
  pinMode(BUZZER, OUTPUT);
}

// LOOP //
void loop() {
  playImperialMarch();
  delay(INTERVAL);
  playStarWarsTheme();
  delay(INTERVAL);
}

